<?php


namespace Formulario;

/**
 * Class UI_Comp_Formulario
 * @package Formulario
 */
class UI_Comp_Formulario
{
    private $params;

    /**
     * UI_Comp_Formulario constructor.
     * @param bool $validateScript
     */
    function __construct($validateScript = false)
    {
        if ($validateScript)
            echo ' <script src="validation.js"></script>';
    }

    /**
     * Retorna html do formulário
     * @param array $param
     */
    function renderer($param = array()) //Também pode ser  $param = false, mas para manter um padrão na tipagem modifiquei para array()
    {
        /* Validações Formulário */
        if (!empty($param)) {
            $this->params = $param;
            $validate = $this->validate();
            if (!$validate) {
                echo "Ocorreu um erro nas validações de campos.";
                return;
            }
        }

        /* Instanciar Valores Iniciais dos Campos */
        if (!$param)
            $param = [
                'data' => '',
                'texto' => '',
                'checkbox' => 'off',
                'texto_grande' => '',
            ];

        if (!isset($param['checkbox']))
            $param['checkbox'] = 'off';

        echo '<form action="index.php" method="post" id="form">
        <fieldset class="box">
            <div class="grid-container">'
            . UI_Fields_Formulario::inputData('data', 'Data', $param['data'])
            . UI_Fields_Formulario::inputText('texto', 'Texto', $param['texto'])
            . UI_Fields_Formulario::checkBox('checkbox', 'Checkbox?', $param['checkbox'])
            . UI_Fields_Formulario::textArea('texto_grande', 'Texto grande', $param['texto_grande'])
            . '<br>'
            . UI_Fields_Formulario::button('submit', 'Submit')
            . '</div>
        </fieldset>
    </form>';
    }

    /**
     * Valida formulário
     * @return bool
     */
    function validate()
    {
        //Apenas valida datas formato mm-dd-YYYY, não verifica se a data é valida. Ex: 11-31-2020
        if (!preg_match("/^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-[0-9]{4}$/", $this->params['data']))
            return false;

        //O texto só deverá possuir letras minúsculas e espaços, até 144 chars.
        if (!preg_match("/^[a-z ]+$/", $this->params['texto'])
            && strlen($this->params['texto']) < 145)
            return false;

        //O texto só deverá possuir letras maiúsculas, números e espaços até 255 chars.
        if (!preg_match("/^[A-Z0-9 ]+$/", $this->params['texto_grande'])
            && strlen($this->params['texto_grande']) < 256)
            return false;

        return true;
    }
}