<?php


namespace Formulario;

/**
 * Class UI_Fields_Formulario
 * Classe para gerar campos em html
 * @package Formulario
 */
class UI_Fields_Formulario
{
    /**
     * @param $name
     * @param $label
     * @param string $value
     * @return string
     */
    public static function inputData($name, $label, $value = '')
    {
        return '<div class="grid-item">
                    <label for="'.$name.'">'.$label.':</label>
                </div>
                <div class="grid-item">
                    <input type="text" id="'.$name.'" name="'.$name.'" value="'.$value.'">
                </div>';
    }

    /**
     * @param $name
     * @param $label
     * @param string $value
     * @return string
     */
    public static function inputText($name, $label, $value = '')
    {
        return '<div class="grid-item">
                    <label for="'.$name.'">'.$label.':</label>
                </div>
                <div class="grid-item">
                    <input type="text" id="'.$name.'" name="'.$name.'" value="'.$value.'">
                </div>';
    }

    /**
     * @param $name
     * @param $label
     * @param string $value
     * @return string
     */
    public static function checkBox($name, $label, $value = 'off')
    {
        $value == 'on'? $checked = 'checked' : $checked = '';
        return '<div class="grid-item">
                    <label for="'.$name.'">'.$label.':</label>
                </div>
                <div class="grid-item checkbox-custom">
                    <input type="checkbox" id="'.$name.'" name="'.$name.'" '.$checked.'>
                </div>';
    }

    /**
     * @param $name
     * @param $label
     * @param string $value
     * @return string
     */
    public static function textArea($name, $label, $value = '')
    {
        return '<div class="grid-item">
                    <label for="'.$name.'">'.$label.':</label>
                </div>
                <div class="grid-item">
                    <textarea name="'.$name.'" id="'.$name.'" rows="8" cols="35">'.$value.'</textarea>
                </div>';
    }

    /**
     * @param $type
     * @param string $value
     * @return string
     */
    public static function button($type, $value = '')
    {
        return '<div class="grid-item">
                    <input type="'.$type.'" value="'.$value.'">
                </div>';
    }

}