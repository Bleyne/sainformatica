# Teste Formulário SA Informatica #

Respositório com solução para o Teste PHP da empresa SA Informatica

### Instruções ###

O teste consiste na criação de um script PHP básico, que possuirá uma classe chamada UI_Comp_Formulario, esta classe deverá possuir as seguintes funções:

* UI_Comp_Formulario($validateScript=false)
Caso o parâmetro $validateScript seja passado, deverá associar ao onsubmit do formulário a ser renderizado validações JavaScript para previnir o submit do form com as seguintes regras:
       - Data: deverá ser um campo de data no seguinte formato mm-dd-YYYY
       - Texto: O texto só deverá possuir letras minúsculas e espaços, até 144 chars.
       - Texto grande: O texto só deverá possuir letras maiúsculas, números e espaços até 255 chars.

* renderer($param=false) 
Função que retornará o html do formulário, seguindo o modelo do layout enviado.
onde $param deverá ser um array com os seus índices iguais ao nome dos campos do formulário, este parâmetro não será obrigatório sendo que se caso não seja passado, os campos do formulário estarão vazios.

* validate() return bool
Esta função irá validar o seu formulário com as seguintes regras:
       - Data: deverá ser um campo de data no seguinte formato mm-dd-YYYY
       - Texto: O texto só deverá possuir letras minúsculas e espaços, até 144 chars.
       - Texto grande: O texto só deverá possuir letras maiúsculas, números e espaços até 255 chars.

O script PHP deverá possuir HTML e CSS seguindo o especificado no layout em anexo.
Dentro de #conteudo instanciar  UI_Comp_Formulario e chamar renderer, caso o formulário seja submetido chamar renderer com $_POST como parâmetro.
  
### Demo ###

https://brunleyne.net/