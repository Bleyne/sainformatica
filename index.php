<?php
require __DIR__ . '/vendor/autoload.php';;
?>
<!doctype html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noindex">
    <title>Teste SA Informática</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="favicon.ico"/>
</head>

<body>
<div class="container">
    <header>
        <h1 class="titulo-pagina container">Formulário de teste</h1>
    </header>
</div>
<hr class="full-width">
<div class="container">
    <!-- Form aqui -->
    <?php
    $teste = new Formulario\UI_Comp_Formulario(true);
    $teste->renderer($_POST);
    ?>
    <br>
    <span class="footer-text">Teste de formulário</span>
</div>
</body>
</html>