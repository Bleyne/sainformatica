function validateDate(value)
{
    let dataRegexValidate = /^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-[0-9]{4}$/;
    return value.match(dataRegexValidate) != null;
}

function validateTexto(value)
{
    let dataRegexValidate = /^[a-z ]+$/;
    return value.match(dataRegexValidate) != null && value.length < 145;
}

function validateTextoGrande(value)
{
    let dataRegexValidate = /^[A-Z0-9 ]+$/;
    return value.match(dataRegexValidate) != null && value.length < 256;
}

function validateFields(event)
{
    let isValid = true;
    let dataValue = document.getElementById("data").value;
    if(!validateDate(dataValue)) {
        alert("Data inválida, formato mm-dd-YYYY");
        isValid = false;
    }

    let textoValue = document.getElementById("texto").value;
    if(!validateTexto(textoValue)) {
        alert("O texto só deverá possuir letras minúsculas e espaços, até 144 chars.");
        isValid = false;
    }

    let textoGrandeValue = document.getElementById("texto_grande").value;
    if(!validateTextoGrande(textoGrandeValue)) {
        alert("O texto grande só deverá possuir letras maiúsculas, números e espaços até 255 chars.");
        isValid = false;
    }

    if (!isValid)
        event.preventDefault();
}

document.addEventListener("DOMContentLoaded", function()
{
    const form = document.getElementById('form');
    form.addEventListener('submit', validateFields);
});